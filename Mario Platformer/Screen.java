import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Screen superclass for all screens displayed in the World
 * Subclass of Actor
 * All screen objects will inherit methods of Screen
 * 
 * @author Jerry Zhu
 * @version December 2020
 */
public class Screen extends Actor
{
    /** 
     * Constructor for objects of class Screen
     */
    public Screen()
    {
        // Empty constructor since no instance variables need to be initialized
    }
    
    /**
     * Act - do whatever the Screen wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Do nothing
    } 
}
