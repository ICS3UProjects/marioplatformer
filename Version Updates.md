# MarioPlatformer 

## TODO: ADD JAR AND GFAR

## Version October 2020 [Complete]

* ~~Get Mario moving~~
* ~~Make Mario fall and hit ground~~
* ~~Jumping gravity and animations~~
* ~~Basic background~~

## Version November 2020 [Complete]

* ~~Goomba enemy~~
* ~~Different ground objects~~
* ~~Platform physics and movement~~
* ~~Level and HUD switching~~

## Version December 2020 [Complete]

* ~~Coins and counters~~
* ~~Death and win animation~~
* ~~Difficulty setting~~
* ~~Title screen~~
* ~~Update paint order and levels~~

## Version December 2020 [Complete]
* ~~Add comments for each line~~
* ~~Add proper formatting~~
* ~~Add documentation~~
* ~~Infinite game loop~~

## Version TBD

* Bullet bill enemy
* Floating/Invisible platform enemy
* Update HUD and levels
